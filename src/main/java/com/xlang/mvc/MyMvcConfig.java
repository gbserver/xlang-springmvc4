/**
 * Xlang.com Inc.
 * Copyright (c) 2004-2017 All Rights Reserved.
 */
package com.xlang.mvc;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

/**
 * 
 * @author xlang
 * @version $Id: MyMvcConfig.java, v 0.1 2017年5月15日 下午10:22:32 xlang Exp $
 */
@Configuration
@EnableWebMvc
@ComponentScan("com.xlang.mvc")
public class MyMvcConfig {
    
    @Bean
    public InternalResourceViewResolver viewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/classes/views/");
        viewResolver.setSuffix(".jsp");
        viewResolver.setViewClass(JstlView.class);
        return viewResolver;
    }

}
